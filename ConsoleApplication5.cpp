﻿#include <iostream>
#include <string>

using namespace std;



class Player {
public:
    string name;
    int score;

    Player(string name, int score) {
        this->name = name;
        this->score = score;
    }
};
//
int main() {
    int numOfPlayers;
    cout << "Enter number of players: ";
    cin >> numOfPlayers;

    Player** players = new Player * [numOfPlayers];
    for (int i = 0; i < numOfPlayers; i++) {
        string name;
        int score;
        cout << "Enter name of player " << i + 1 << ": ";
        cin >> name;
        cout << "Enter score of player " << i + 1 << ": ";
        cin >> score;

        players[i] = new Player(name, score);
    }

    for (int i = 0; i < numOfPlayers; i++) {
        for (int j = 0; j < numOfPlayers; j++) {
            if (players[i]->score > players[j]->score) {
                Player* temp = players[i];
                players[i] = players[j];
                players[j] = temp;
            }
        }
    }


    cout << "Players:" << endl;
    for (int i = 0; i < numOfPlayers; i++) {
        cout << players[i]->name << " - " << players[i]->score << endl;
    }


    for (int i = 0; i < numOfPlayers; i++) {
        delete players[i];
    }
    delete[] players;
    
    return 0;
}
  

